<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <?php require 'View/parts/header.php'; ?>
    <h1>Les planètes !!</h1>
    <a href="index.php?controller=default&action=home">Revenir en arrière</a>
    <br>
    <a href="index.php?controller=planet&action=ajout">
        <button class="btn btn-success">Ajouter une planète</button>
    </a>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom</th>
            <th scope="col">Description</th>
            <th scope="col">Terrain</th>
            <th scope="col">Picture</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach ($planets as $planet){
        ?>
        <tr>
            <th scope="row"><?php echo($planet->getId()) ?></th>
            <td><?php echo($planet->getNom()) ?></td>
            <td><?php echo($planet->getDescription()) ?></td>
            <td><?php echo($planet->getTerrain()) ?></td>
            <td><img style="max-height: 50px" src="public/img/<?php echo($planet->getPicture()) ?>" alt="une planete"></td>
            <td>
                <a href="index.php?controller=planet&action=detail&id=<?php echo($planet->getId());?>">
                    Voir <?php echo($planet->getNom());?></a>
                <br>
                <a href="index.php?controller=planet&action=update&id=<?php echo($planet->getId());?>">Mettre à jour la planète</a>
                <br>
                <a href="index.php?controller=planet&action=delete&id=<?php echo($planet->getId());?>">Supprimer la planète</a>
            </td>
        </tr>
        <?php
            }
            ?>
        </tbody>
    </table>
    <?php require 'View/parts/footer.php'; ?>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>