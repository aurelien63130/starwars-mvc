<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <?php require 'View/parts/header.php'; ?>
    <h1>Ajouter un Vaisseau !!</h1>

    <a href="index.php?controller=planet&action=list">Retour</a>

    <form method="post" class="row">
        <div class="col-md-12">
            <label for="nom" class="form-label">Nom</label>
            <input type="text"
                   value="<?php if(array_key_exists("nom", $_POST)){echo($_POST["nom"]);};?>"
                   name="nom" class="form-control
            <?php  if(array_key_exists("nom", $errors)){echo('is-invalid');} ?>"
                   id="nom">

            <div id="validateNom" class="invalid-feedback">
                <?php if(array_key_exists("nom", $errors)){echo($errors["nom"]);}?>
            </div>
        </div>

        <div class="col-md-12">
            <label for="taille">
                Taille
            </label>
            <input type="number" class="form-control
            <?php  if(array_key_exists("taille", $errors)){echo('is-invalid');} ?>"
                   value="<?php if(array_key_exists("taille", $_POST)){echo($_POST["taille"]);};?>"
                   step="0.01" id="taille" name="taille">
            <div id="validateNom" class="invalid-feedback">
                <?php if(array_key_exists("taille", $errors)){echo($errors["taille"]);}?>
            </div>
        </div>

        <div class="col-md-12">
            <label for="fonction">
                Fonction
            </label>
            <select
                    class="form-select <?php  if(array_key_exists("fonction", $errors)){echo('is-invalid');} ?>" name="fonction" id="fonction">
                <?php foreach (StarshipController::$fonctions as $fonction){
                    $selected = '';
                    if($_POST["fonction"] == $fonction){
                        $selected = 'selected';
                    }
                    echo('<option '.$selected.' value="'.$fonction.'">'.$fonction.'</option>');
                }
                ?>
            </select>

            <div id="validateNom" class="invalid-feedback">
                <?php if(array_key_exists("fonction", $errors)){echo($errors["fonction"]);}?>
            </div>


        </div>

        <div class="col-md-12">
            <label for="picture" class="form-label">Photo</label>
            <input  value="<?php if(array_key_exists("picture", $_POST)){echo($_POST["picture"]);};?>" type="text" name="picture" class="form-control
            <?php  if(array_key_exists("picture", $errors)){echo('is-invalid');}?>" id="picture">
            <div class="invalid-feedback">
                <?php   if(array_key_exists("picture", $errors)){echo($errors["picture"]);}?>
            </div>
        </div>


        <input type="submit" class="btn btn-success m-2">

    </form>

    <?php require 'View/parts/footer.php'; ?>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>