<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <?php require 'View/parts/header.php'; ?>
    <h1>Les vaisseaux !!</h1>
    <a href="index.php?controller=default&action=home">Revenir en arrière</a>
    <br>
    <a href="index.php?controller=vaisseaux&action=add">Ajouter un vaisseau</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom</th>
            <th scope="col">Taille</th>
            <th scope="col">Fonction</th>
            <th scope="col">Picture</th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach ($starships as $starship){
                ?>
                <tr>
                    <td><?php echo($starship->getId()); ?></td>
                    <td><?php echo($starship->getNom()); ?></td>
                    <td><?php echo($starship->getTaille()); ?></td>
                    <td><?php echo($starship->getFonction()); ?></td>
                    <td><img style="max-width: 50px;" src="<?php echo($starship->getPicture());?>"></td>
                </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
    <?php require 'View/parts/footer.php'; ?>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>