<!--<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">
        <img src="public/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
        StarwarsApp
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="index.php?controller=planet&action=list">Les planètes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?controller=vaisseaux&action=list">Les vaisseaux</a>
            </li>
        </ul>
    </div>
</nav>-->

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="public/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
            StarwarsApp
        </a>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php?controller=planet&action=list">Les planètes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?controller=vaisseaux&action=list">Les vaisseaux</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="index.php?controller=security&action=logout">Me déconecter</a>
                </li>
            </ul>
        </div>

        <?php
        if($this->currentUser){
            echo('Bonjour '.$this->currentUser->getNom(). ' ' . $this->currentUser->getPrenom());
        }

        ?>
    </div>
</nav>