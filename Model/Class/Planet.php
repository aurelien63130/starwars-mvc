<?php
    class Planet{
        private $id;
        private $nom;
        private $description;
        private $terrain;
        private $picture;

        public function __construct(?int $id,
                                    string $nom,
                                    ?string $description,
                                    ?string $terrain,
                                    ?string $picture){
            $this->id = $id;
            $this->nom = $nom;
            $this->description = $description;
            $this->terrain = $terrain;
            $this->picture = $picture;
        }

        public function getId(): int{
            return $this->id;
        }

        public function setId($id): void {
            $this->id = $id;
        }

        public function getNom(): string {
            return  $this->nom;
        }

        public function setNom(string $nom): void{
            $this->nom = $nom;
        }

        public function getDescription(): string {
            return  $this->description;
        }

        public function setDescription(string $description): void {
            $this->description = $description;
        }

        public function getTerrain(): ?string {
            return $this->terrain;
        }

        public function setTerrain(string $terrain): void{
            $this->terrain = $terrain;
        }

        public function getPicture(): string {
            return  $this->picture;
        }

        public function setPicture(string $picture): void{
            $this->picture  = $picture;
        }
    }
?>