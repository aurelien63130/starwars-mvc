<?php
class Starship {
    private $id;
    private $nom;
    private $picture;
    private $taille;
    private $fonction;


    public function __construct($id, $nom, $picture, $taille, $fonction)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->picture = $picture;
        $this->taille = $taille;
        $this->fonction = $fonction;
    }


    public function getId()
    {
        return $this->id;
    }


    public function setId($id): void
    {
        $this->id = $id;
    }


    public function getNom()
    {
        return $this->nom;
    }


    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    public function getPicture()
    {
        return $this->picture;
    }

    public function setPicture($picture): void
    {
        $this->picture = $picture;
    }

    public function getTaille()
    {
        return $this->taille;
    }

    public function setTaille($taille): void
    {
        $this->taille = $taille;
    }

    public function getFonction()
    {
        return $this->fonction;
    }

    public function setFonction($fonction): void
    {
        $this->fonction = $fonction;
    }



}