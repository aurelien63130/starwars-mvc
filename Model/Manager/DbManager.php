<?php
abstract class DbManager {
    protected $pdo;
    private $host = 'database';
    private $user = 'root';
    private $password = 'tiger';
    private $dbName = 'starwars';

    public function __construct(){
        try {
            $this->pdo = new PDO("mysql:dbname=".$this->dbName.";host=".$this->host,
                $this->user,$this->password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e){
            throw $e;
            die();
        }
    }
}