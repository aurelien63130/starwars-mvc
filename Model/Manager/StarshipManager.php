<?php
    class StarshipManager extends DbManager {
        public function getAll(){
            $query = $this->pdo->prepare("SELECT * FROM starship");
            $query->execute();
            $res = $query->fetchAll();

            $starships = [];

            foreach ($res as $vaisseau){
                $starships[] = new Starship($vaisseau["id"], $vaisseau["nom"],
                    $vaisseau["picture"], $vaisseau["taille"],
                    $vaisseau["fonction"]);
            }

            return $starships;
        }

        public function add(Starship $starship): void{
            $nom = $starship->getNom();
            $taille = $starship->getTaille();
            $picture = $starship->getPicture();
            $fonction = $starship->getFonction();

            $query = $this->pdo->prepare(
                "INSERT INTO starship (nom, taille, picture, fonction)
                        VALUES (:nom, :taille, :picture, :fonction)");
            $query->bindParam("nom", $nom);
            $query->bindParam("taille", $taille);
            $query->bindParam("picture", $picture);
            $query->bindParam("fonction", $fonction);

            $query->execute();
        }
    }
?>