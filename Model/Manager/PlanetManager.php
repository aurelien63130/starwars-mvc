<?php
// Planet Manager
// Cette classe aura le role de transformer nos requêtes MySQL en objet (Selection)
// Elle devra aussi transformer nos objets en requête mysql (insert / update)
// Cette classe elle étend de la classe abstraite DbManager qui contient la connection
// a notre BDD (attribut PDO)
class PlanetManager extends DbManager {

    // Ici, nous avons mèthode getAll
    public function getAll() {
        // Elle requête toutes les planètes dans notre BDD
        $query = $this->pdo->prepare("SELECT * FROM planet");
        $query->execute();
        // On reccupére ici nos resultats sous forme de tableau
        $results = $query->fetchAll();

        // On cré un tableau vide qui contiendra nos objets
        $planets = [];

        // On parcours nos resultats, on les tranforme en objet
        foreach ($results as $res){
            // On ajoute ces objets dans notre tableau créé à la ligne 17
            $planets[] = new Planet($res['id'], $res['nom'],
                $res['description'],
                $res['terrain'],
                $res['picture']);
        }

        // On retourne notre tableau contenant nos objets !!!
        return $planets;
    }

    public function getOne($id){
        $query =
            $this->pdo->prepare("SELECT * FROM planet WHERE id = :id");
        $query->bindParam(':id', $id);
        $query->execute();
        $res = $query->fetch();

        $planet = null;
        if($res){
            $planet = new Planet($res['id'], $res['nom'], $res['description'],
                $res["terrain"], $res["picture"]);
        }

        return $planet;
    }

    public function update(Planet $planet){
        $nom = $planet->getNom();
        $description = $planet->getDescription();
        $terrain = $planet->getTerrain();
        $picture = $planet->getPicture();
        $id = $planet->getId();

        $query = $this->pdo->prepare("UPDATE planet SET
        nom = :nom, 
        description = :description,
        terrain = :terrain,
        picture = :picture
        WHERE id = :id");

        $query->bindParam("nom", $nom);
        $query->bindParam('description', $description);
        $query->bindParam("terrain", $terrain);
        $query->bindParam("picture", $picture);
        $query->bindParam("id", $id);

        $query->execute();
    }

    public function add(Planet $planet) {
        $nom = $planet->getNom();
        $description = $planet->getDescription();
        $terrain = $planet->getTerrain();
        $picture = $planet->getPicture();

        $query = $this->pdo->prepare(
            "INSERT INTO planet (nom, description, terrain, picture) VALUES
                    (:nom, :description, :terrain, :picture)");

        $query->execute(
            [
                "nom"=>$nom,
                "description"=> $description,
                "terrain"=> $terrain,
                "picture"=> $picture]);

        $planet->setId($this->pdo->lastInsertId());

        return $planet;

    }

    public function delete($id){
        $query = $this->pdo->prepare("DELETE FROM planet WHERE id=:id");
        $query->bindParam("id", $id);
        $query->execute();
    }
}