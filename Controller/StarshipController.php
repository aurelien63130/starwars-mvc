<?php
    class StarshipController extends SecurityController {

        private $starshipManager;

        public function __construct()
        {
            parent::__construct();
            parent::isLoggedIn();
            $this->starshipManager = new StarshipManager();
        }

        public static $fonctions = [
            "",
            "Transport de Marchandises",
            "Véhicule d'Assaut Lourd"
        ];

        public function displayAll(){
            $starships = $this->starshipManager->getAll();

           require 'View/starships/list.php';

        }

        public function ajout(){
            $errors = [];

            if($_SERVER["REQUEST_METHOD"] == 'POST'){
                if(strlen($_POST["nom"]) > 250){
                    $errors["nom"] = "Le nom est saisi est trop grand (250 caractères)";
                }
                if(strlen($_POST["picture"])> 250){
                    $errors["picture"] = "Le lien de la photo est trop grand (250 caractères)";
                }
                if(!is_numeric($_POST["taille"])){
                    $errors["taille"] = 'Veuillez saisir un nombre';
                }
                if(!in_array($_POST['fonction'], self::$fonctions)) {
                    $errors["fonction"] = "Cette fonction n'existe pas";
                }

                if(count($errors) == 0){
                    $starship =
                        new Starship(null, $_POST["nom"], $_POST["picture"], $_POST["taille"], $_POST["fonction"]);

                    $this->starshipManager->add($starship);

                    header("Location: index.php?controller=vaisseaux&action=list");
                }
            }

            require 'View/starships/form-add.php';
        }
    }
?>