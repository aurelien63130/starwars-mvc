<?php
class PlanetController extends SecurityController {
    private $pm;

    public static $allowedTerrain = [
        "Cailloux",
        "Sable",
        "Terre",
        "Glace"
    ];

    public static $allowedPicture = [
        "image/jpeg",
        "image/png"
    ];

    public function __construct(){
        parent::__construct();
        parent::isLoggedIn();
        $this->pm = new PlanetManager();
    }

    public function displayAll(){
        $planets = $this->pm->getAll();

        require 'View/planets/list.php';
    }



    public function displayOne($id){
        $planet = $this->pm->getOne($id);

        if(is_null($planet)){
            header("Location: index.php?controller=default&action=not-found&scope=planet");
        }

        require 'View/planets/detail.php';
    }

    public function ajout(){

        $errors = [];

        if($_SERVER["REQUEST_METHOD"] == 'POST'){
            // Vérifier mon formulaire
            $errors = $this->checkForm();

            if(count($errors) == 0){
                $uniqFileName = null;
                // Ici je vais gérer l'upload de fichier
                if(!in_array($_FILES["picture"]["type"], self::$allowedPicture)){
                    $errors["picture"] = "Je n'accepte ce fichier veuillez ajouter une image";
                }
                if($_FILES["picture"]["error"] != 0){
                    $errors["picture"] = 'Erreur de l\'upload';
                }
                if($_FILES["picture"]["size"] > 1000000){
                    $errors["picture"] = "Le fichier est trop gros";
                }

                if(count($errors) == 0){
                    $extension = explode('/',$_FILES["picture"]["type"])[1];
                    $uniqFileName = uniqid().'.'.$extension;
                    move_uploaded_file($_FILES["picture"]["tmp_name"], "public/img/".$uniqFileName);
                }


                $planet = new Planet(null, $_POST["nom"],
                    $_POST["description"], $_POST["terrain"], $uniqFileName);
                $this->pm->add($planet);
                header("Location: index.php?controller=planet&action=list");
            }
            // Si il est valide je vais enregiestrer mes données puis rediriger l'utilisateur
        }

        require "View/planets/form-add.php";
    }

    private function checkForm(){
        $errors = [];
        if(empty($_POST["nom"])){
            $errors["nom"] = 'Veuillez saisir le nom de la planète';
        }

        if(strlen($_POST["nom"])>250){
            $errors["nom"] = "Le nom est trop long (250 caractères maximum)";
        }

        if(!in_array($_POST["terrain"], self::$allowedTerrain)){
            $errors["terrain"] = "Ce type de terrain n'existe pas";
        }


        return $errors;
    }

    public function update($id){
        $errors = [];
        $planet = $this->pm->getOne($id);

        if(is_null($planet)){
            header('Location: index.php?controller=default&action=not-found&scope=planet');
        } else {
            if($_SERVER["REQUEST_METHOD"] == 'POST'){
                $errors = $this->checkForm();

                $planet->setNom($_POST["nom"]);
                $planet->setDescription($_POST["description"]);
                $planet->setTerrain($_POST["terrain"]);

                if(count($errors) == 0){
                    if(array_key_exists("picture",$_FILES)){
                        $uniqFileName = null;
                        // Ici je vais gérer l'upload de fichier
                        if(!in_array($_FILES["picture"]["type"], self::$allowedPicture)){
                            $errors["picture"] = "Je n'accepte ce fichier veuillez ajouter une image";
                        }
                        if($_FILES["picture"]["error"] != 0){
                            $errors["picture"] = 'Erreur de l\'upload';
                        }
                        if($_FILES["picture"]["size"] > 1000000){
                            $errors["picture"] = "Le fichier est trop gros";
                        }

                        if(count($errors) == 0){
                            unlink("public/img/".$planet->getPicture());
                            $extension = explode('/',$_FILES["picture"]["type"])[1];
                            $uniqFileName = uniqid().'.'.$extension;
                            move_uploaded_file($_FILES["picture"]["tmp_name"], "public/img/".$uniqFileName);
                            $planet->setPicture($uniqFileName);
                        }
                    }

                    $this->pm->update($planet);
                    // rediriger l'utilisateur
                    header("Location: index.php?controller=planet&action=list");
                }
            }

            require 'View/planets/form-edit.php';
        }
    }

        public function delete($id){
            $planet = $this->pm->getOne($id);

            if(is_null($planet)){
                header('Location: index.php?controller=default&action=not-found&scope=planet');
            } else {
                unlink("public/img/".$planet->getPicture());
                $this->pm->delete($planet->getId());
                header("Location: index.php?controller=planet&action=list");
            }


        }

}